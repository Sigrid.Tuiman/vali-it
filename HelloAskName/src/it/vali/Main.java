
package it.vali;

// import tähendab, et antud klassile Main lisatakse ligipääs java class libriary paketile
// java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit
        // lugeda
        Scanner scanner = new Scanner ( System.in);

        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Mis on sinu lemmikvärv?");
        String color = scanner.nextLine();

        System.out.println("Mis  autoga sa sõidad ?");
        String car = scanner.nextLine();
        // Küsi mis on sinu lemmik värv
        // Mis autoga sa sõidad

        System.out.println("Tere" + " " + name + " " + color+ " " + " "+car);

        System.out.printf("Tere %s  %s  %s  \n", name, color, car);

        StringBuilder builder = new StringBuilder();
        builder. append("Tere");
        builder.append (name);
        builder.append (color);
        builder.append (car);
        builder.append (name);
        builder.append (color);
        builder.append (car);

        String fullText = builder.toString();

        System.out.println(fullText);

        // Nii system.out printf kui ka String.format kasutavad enda siseselt StringBuilderit

        String text = String. format("Tere %s  %s  %s  \n", name, color, car);

        System.out.println(text);




    }
}
