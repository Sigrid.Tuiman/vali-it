package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int a = 100;
        short b = (short) a;

        a = b;
        double c = a;
        a = (int) c;

        // Iga kassi võib võtta kui looma
        Animal animal = new Cat();


        // Iga loom ei ole klass
        // explicit casting
        Cat cat = (Cat) animal;


        List<Animal> animals = new ArrayList<Animal>();


        Dog dog = new Dog();
        Elk elk = new Elk();

        animals.add(dog);
//    animals.add(elk);
//    animals.add(new Lion());
        animals.add(new Pet());
        animals.get(0).setName(("Jaan"));

           animals.get(animals.indexOf(dog)).setName("Peeter") ;

//

        // Kutsu kõikide listis olevate loomade printInfo välja
        for (Animal animalInList : animals) {
            animalInList.printInfo();
            System.out.println();

        }
        for (Animal animalInList: animals) {
            animalInList.eat();
            System.out.println();

        }

        // teisendama tagasi, selleks sulud
        Animal secondAnimal = new Dog();
        ((Dog)secondAnimal).setHasTail(true);
        secondAnimal.printInfo();



    }
}

