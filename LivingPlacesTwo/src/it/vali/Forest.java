package it.vali;

import java.util.Map;

public class Forest implements LivingPlace {

    @Override
    public void addAnimal(Animal animal) {
        if (!WildAnimal.class.isInstance(animal)) {
            System.out.println("Metsas elavad ainult metsloomad");
            return;


        }

        @Override
        public void printAnimalCounts () {
            for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
                System.out.println(entry.getKey() + " " + entry.getValue());
            }

        }

        @Override
        public void removeAnimal (String animalType){

            for (WildAnimal wildAnimal : animals) {
                if (wildAnimal.getClass().getSimpleName().equals(animalType)) {
                    animals.remove(wildAnimal);
                    System.out.printf("Metsast eemaldati loom%s%n", animalType);


                }
            }
        }
    }
}




//    boolean animalFound = false;
//
//        for (FarmAnimal farmAnimal : animals) {
//                if(farmAnimal.getClass().getSimpleName().equals(animalType)) {
//                animals.remove(farmAnimal);
//                System.out.printf("Farmist eemaldati loom %s%n", animalType);
//
//                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
//                // muuljuhul vähenda animalCounts mapis seda kogust
//                if(animalCounts.get(animalType) == 1) {
//                animalCounts.remove(animalType);
//                } else {
//                animalCounts.put(animalType, animalCounts.get(animalType) - 1);
//                }
//
//                animalFound = true;
//                break;
//                }
//                }
//                if(!animalFound) {
//                System.out.println("Farmis antud loom puudub");
//                }



//    public void printAnimalCounts() {
//        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
//            System.out.println(entry.getKey() + " " + entry.getValue());
//    @Override
//    public void addAnimal(Animal animal) {
//        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
//        if(!FarmAnimal.class.isInstance(animal)) {
//            System.out.println("Farmis saavad elada ainult farmi loomad");
//            return;
//        }

// Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende
// 3 meetodi sisud