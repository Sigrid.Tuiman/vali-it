package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {GAS,PETROL, DIESEL, HYBRID, ELECTRIC
}


public class Car {
    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private boolean isEngineRunning;
    private int speed;
    private int maxSpeed;

    private Person owner;
    private Person driver;

    private int maxPassengers;


    private List<Person> passengers = new ArrayList<Person>();





    // Konstruktor constructor
    // on eriline meetod, mis käivitatakse klassist objekti loomisel
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
        fuel = Fuel.PETROL;

    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(String model, int maxPassenger){
        this.model = model;
        this.maxPassengers = maxPassenger;

    }

    public Car(String make, String model, int year, int maxSpeed, Fuel fuel) {
        this.make = make;


    }

    public void startEngine() {
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", targetSpeed);
        }
    }
// slowDown (int targetSpeed)
    // park() mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid) aeglustama nullini, mootori välja lülitama
    // Lisa autole parameetrid driver ja owner (tüübist Person) ja nendele siis get ja set meetodid
    // Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik
    // Prindi välja auto omaniku vanus


    public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");

        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivna");
        } else if (targetSpeed > 0){
            System.out.println("vajuta pidur");
        }

    }
public void parking(){
        slowDown(0);
        stopEngine();
}

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    public int getMaxPassenger() {
        return maxPassengers;
    }

    public void setMaxPassenger(int maxPassenger) {
        this.maxPassengers = maxPassenger;
    }

    public void addPassanger(Person passanger) {
        if(passengers.size()< maxPassengers){
            if(passengers.contains(passanger))

            System.out.printf("Autosse juba on  autos %s%n", passanger.getFirstName());
        }


        else {
            passengers.add(passanger);
            System.out.println();



            System.out.println("Autosse ei mahu enam reisijaid");
        }


    }
    public  void removePassangers(Person passenger){

        if(passengers.indexOf(passenger) != -1){
            System.out.println();
        }
        passengers.remove(passenger);

    }else {
        System.out.println("Autos sellist reisijat pole");
    }

    // Foreach loop
    //Iga elemendi kohta listis passengers tekita objekt passenger
    // 1. kordus Person passenger on esimene rreisija
    // 2. kordus Person passenger on teine rreisija


    public void showPassengers(){
        System.out.println("Autos on järgmised reisijad:");

        for(Person passenger : passengers){
            System.out.println(passenger.getFirstName());
        }
        for (int i = 0; i < passengers.size ; i++) {
            System.out.println(passengers.get(i).getFirstName());

        }
    }
    // Lisa autole max reisijate arv

    //Lisa autole võimalus hoida reisijaid
    //Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisataks rohkem reisijaid kui mahub



}