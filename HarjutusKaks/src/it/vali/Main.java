package it.vali;


public class Main {
    public static void main(String[] args) {
        int[] numbers = new int[5]; // 5 tühja auku, mis on vaja initsialiseerida
        numbers[0] = 10;
        String[] strings = new String[3];      // String - andmete tüüp [] - massiiv 3 objekti klassist String
                                            // klassi objekt on lause
        strings[0] = "tere"; // augu strings initsialiseerimine kohal 0 sõnaga tere
        strings[1] = "tsau";
        strings[2] = "java";

        System.out.println();
        // for each tsükkel
        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }
        System.out.println();
        // ekraanile toomine võimalus, mall
        for(String string: strings){    //String - tüüp(lause tüüp) string -muutuja
                                        // String string: - tsükli sees tahan töötada lausega
                                        //lause nimi on string
                                        // strings on massiiv
            System.out.println(string);
        }
        int[] numbers1 = {1, 2, 3}; // massiiv valmis andmetega 1 2 3
        int sum = 0;                // summa leidmine, muutuja, mis võrdub 0-ga
        for(int x:numbers1){        // x - x asemel võib olla üksküik mis sõna, int x: - muutuja
            sum = sum+x;            // summale lisame x iga kord kui läheb järgmisesse auku
        }                           // Lugemine - esimesel ringil int x on 1, teisel ringil int x= 2
        System.out.println();
        System.out.println(sum);    // vastus 6 - mis on 1+2+3


    }
}


