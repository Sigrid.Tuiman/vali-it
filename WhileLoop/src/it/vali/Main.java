package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {

// while on tsükkel, kus korduste arv ei ole teada

    // Lõpmatu tsükkel kasutades while
//	while (true){
//	    System.out.println("Tere");

       //  Programm mõtleb numbri
        // Programm ütleb kasutajale:" Arva number ära"
        // Senikaua kuni kasutaja arvab valesti, ütleb: "Proovi uuesti"

        Random random = new Random ();
        // random.nextInt(bound:5) genereerib numbri 0 kuni 4

        Scanner scanner = new Scanner(System.in);
        do {
            int number = random.nextInt(5) + 1;
            // 80 -100
            // int number = random.nextInt(20)+80;

            int randomNum = ThreadLocalRandom.current().nextInt(1, 6);

            System.out.println("Arva ära üks number");


            int enteredNumber = Integer.parseInt(scanner.nextLine());
            while (enteredNumber != number){
                System.out.println("Proovi uuesti");
                enteredNumber = Integer.parseInt(scanner.nextLine());
            }

            System.out.println("Tubli! Arvasid numbri ära!");
            System.out.println("Kas soovid veel mängida?");

        } while (!scanner.nextLine().toLowerCase().equals("ei"));

    }
}

