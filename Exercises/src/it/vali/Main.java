package it.vali;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        // 1. Arvuta ringi pindala, kui teada on raadius
        // Prindi pindala välja erkaanile
        double radius = 24.34;
        double area = circleArea(radius);
        System.out.printf("Ringi pindala on %.2f%n", area);

//        int r = 3;
//        System.out.printf("Pindala on ")+ Math.PI+Math.pow(r, 2);

        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
        // ja mille sisendparameetriteks on kaks stringi.
        // Meetod tagastab kas tõene või vale selle kohta,
        // kas stringid on võrdsed

        String firstString = "kala";
        String secondString = "maja";
        boolean areEqual = equals(firstString, secondString);
        System.out.printf("Sõnad %s ja %s on võrdsed: %s%n", firstString, secondString, areEqual);
//        if(areEqual) {
//            System.out.println("Sõnad on võrdsed");
//        }
//        else {
//            System.out.println("Sõnad ei ole võrdsed");
//        }


        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
        // massiiv ja mis tagastab stringide massivi.
        // Iga sisend-massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
        // 3, 6, 7
        // "aaa", "aaaaaaa", "aaaaaaa"
        int[] numbers = new int[] {3, 6, 7};
        String[] words = intArrayToStringArray(numbers);
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        // 4. Kirjuta meetod, mis võtab sisendaparameetrina aastaarvu
        // ja tagastab kõik sellel sajandil esinenud liigaastad
        // Sisestada saab ainult aastaid vahemikus 500-2019.
        // Ütle veateade, kui aastaarv ei mahu vahemikku.

        List<Integer> years = leapYearsInCentury(1400);

        for (int year : years) {
            System.out.println(year);
        }

        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");
        countryNames.add("India");


        language.setCountryNames(countryNames);
        System.out.println(language.toString());



        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
        // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
        // riikide nimekirja eraldades komaga.
        // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetaga ning prindi välja
        // selle objekti toString() meetodi sisu.
    }
//Ülesanne 1
    static double circleArea(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }

//    üleanne 2
    static boolean equals(String firstString, String secondString) {
        if(firstString.equals(secondString)) {
            return true;
        }
        return false;
        // return firstString.equals(secondString);
    }
// Ülesane 3
    static String[] intArrayToStringArray(int[] numbers) {
        String[] words = new String[numbers.length];
//        numbers[0] = 3; words[0] = "aaa";
//        numbers[1] = 4; words[1] = "aaaa";
//        numbers[2] = 7; words[2] = "aaaaaaa";

        for (int i = 0; i < words.length ; i++) {
            words[i] = genereteAString(numbers[i]);
        }

        return  words;
    }

    static String genereteAString(int count) {
        String word = "";

        for (int i = 0; i < count; i++) {
            word += "a";
        }

        return word;
    }
    // 1799
    // start 1700
    // end 1800
    static List<Integer> leapYearsInCentury(int year) {
        List<Integer> years = new ArrayList<Integer>();

        if(year< 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return years;
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        for (int i = 2020; i >= centuryStart ; i -= 4) {
            if(i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }
        return years;
    }

//    Ülesanne 5


    // 1. Arvuta ringi pindala, kui teada on raadius
    // Prindi pindala välja rkraanile
    //Math.pow()
    //2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse
    // ja mille sisendparameetriks on kaks stringi.
    // Meetod tagastab kas tõene või vale selle kohta,
    // kas stringid on võrdsed
    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude
    // massiiv ja mis tagastab stringide massiivi.
    // Iga sisend - massiivi elemendi kohta olgu tagastatavas masssiivis samapalju a tähti
    // 3,6,7,
    // "aaa", "aaaaaa", "aaaaaaa"
    // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
    // ja tagastab kõik sellel sajandil esinenud liigaastad
    //  sisestada saab ainult aastaid vahemikus 500-2019.
    //  Ütle veateade, kui aastaarv ei mahu vahemikku.
    // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega
    // kus seda keelt räägitakse. Kirjuta üle selle klassi meetod toString() nii, et see tagastab
    // riikide nimekirja eraldades komaga.
    // Tekita antud klassist 1 objekt ühe vabalt valitud keele andmetega ning prindi völja
    // selle objekti toString() meetodi sisu.



}








