package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Do while tsükkel o nnagu while tsükkel ainult et kontroll tehakse
        //

//	System.out.println("Kas tahad jätkata? Jah/ei");
//
//	// Jätkame seni, kuni kasutaja kirjutab "ei"
//
//        Scanner scanner = new Scanner(System.in);
//        String answer =scanner.nextLine();
//
//        while (!answer.equals ("ei")) {
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            answer =scanner.nextLine();
//        }
        Scanner scanner = new Scanner(System.in);
        String answer;
        // iga muutuja, mille me deklareerime kehtib ainult seda ümbritsevate loogsulgude {} sees
        do {
            System.out.println("Kas tahad jätkata? Jah/ei");
           answer =scanner.nextLine();

        }
        while (!answer.equals("ei"));
        System.out.println("Proovi uuesti");

        while (!answer.equals("ei"));
        System.out.println("Arvasid õigesti");

    }
}
