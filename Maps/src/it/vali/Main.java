package it.vali;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	 Map<String, String> map = new HashMap<String, String>();

	 // Sõnaraamat
        // Maja => House
        // Isa => Tree
        // Puu => Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        // Map järjekorda ei salvesta
        for(Map.Entry<String, String>entry: map.entrySet()){
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

        // Oletame, et tahan teada, mis on inglise keeles Puu

        String translation = map.get("Puu");
        System.out.println(translation);

        // Isikukood, telefoninumbrid hoitakse stringina
        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("3850233234553", "Lauri");
        idNumberName.put("3850233234444", "Malle");
        idNumberName.put("3850233234444", "Kalle");

        // Kui kasutada put sama key lisamisel, kirjutatakse value üle

        System.out.println(idNumberName.get("3850233234553"));
        idNumberName.remove("3850233234444");
        System.out.println(idNumberName.get("3850233234444"));

        // EST => Estonia
        // Estonia => + 372

        // Loe lauses üle kõik erinevad tähed
        // ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // Char on selline tüüp, kus saab hoida üksikut sümbolit

        char symbolA = 'a';


        String sentence = "elas metsas mutionu";
        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();

        for (int i = 0; i < characters.length ; i++) {
            System.out.println(characters[i]);
            if(letterCounts.containsKey(characters[i])){
                letterCounts.put(characters[i], letterCounts.get(characters[i] + 1));
            }else {
                letterCounts.put(characters[i], 1);

            }

        }

        System.out.println(letterCounts);
        // Map .Entry<Character, Integer> on klass, mis hoiab endas ühte rida map-is
        // ehk ühte key-value paari

       for(Map.Entry<Character, Integer> entry: letterCounts.entrySet()){
           System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue());

        }






        // e 2
        // l 1
        // a 2
        // s 3







    }





//
//    Map<String, String> linkedMap = new LinkedHashMapString, Sti<String, String>();
//
//    // Sõnaraamat
//    // key =< value
//    // Maja => House
//    // Isa => Tree
//    // Puu => Tree
//
//        map.put("Maja", "House");
//        map.put("Isa", "Dad");
//        map.put("Puu", "Tree");
//        map.put("Sinine", "Blue");
//
//    // Map järjekorda ei salvesta
//        for(Map.Entry<String, String>entry: map.entrySet()){
//        System.out.println(entry.getKey() + " " + entry.getValue());
//    }
}
