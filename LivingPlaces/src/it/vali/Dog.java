package it.vali;

public class Dog extends Pet {
    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    private boolean hasTail = true;


    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName());
    }

    @Override
    public void eat() {
        System.out.println("Närin konti");
    }

    @Override
    public int getAge() {
        int age = super.getAge();
        if(age == 0) {
            return 1;
        }
        return age;
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Saba olemas: %s%n", hasTail);
    }
}





//
//package it.vali;
//
//public class Dog extends Animal {
//
//   private boolean hasTail = true;
//
//
//
//
//
//    public void playWithCat(Cat cat){
//        System.out.printf("Mängin kassiga %s%n", cat.getName());
//    }
//
//
//    public boolean isHasTail() {
//        return hasTail;
//    }
//
//    public void setHasTail(boolean hasTail) {
//        this.hasTail = hasTail;
//    }
//}
