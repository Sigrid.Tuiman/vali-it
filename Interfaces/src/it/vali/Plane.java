package it.vali;

// Plane pärineb klassist
public class Plane implements Flyer {
    @Override
    public void fly() {
        doChecklist();
        startEngine();
        System.out.println("Lennuk lendab");
    }

    private void doChecklist() {
        System.out.println("Tehakse checklist");
    }

    private void startEngine() {
        System.out.println("Mootor käivitus");

    }
}

