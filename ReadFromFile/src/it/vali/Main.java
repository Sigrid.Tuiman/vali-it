package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
	FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents.input.txt");
	BufferedReader bufferedReader = new BufferedReader(fileReader);


            String line = bufferedReader.readLine();
            System.out.println(line);

            // Kui readLine
            while(line != null){
                System.out.println(line);
                line = bufferedReader.readLine();

            }

            // do teeb sama, mis while e.eelmine  näide
            String line = null;
            do {
               System.out.println();

            }while()



            bufferedReader.close();
            fileReader.close();

    }
        catch(FileNotFoundException e){
            e.printStackTrace();

            }

        catch(IOException e) {
                e.printStackTrace();
        }


    }
}
