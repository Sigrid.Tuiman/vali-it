﻿-- See on lihtne hello world teksti päring, mis 
-- tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). 
-- Selles veerus ja reas saab olema tekst Hello world
SELECT 'Hello World';
-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;
-- ei tööta PostgreSQL'is. Näiteks MSSql'is töötab
SELECT 'raud' + 'tee' 
-- Standard CONCAT töötab kõigis erinevates SQL serverites 
-- liidab sõnu ja numbreid,'.' tähendab tühimik,  väljatrükk allmaaraudteejaam 2004 
SELECT CONCAT('all', 'maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4);

-- Kui tahan erinevaid veege, siis panen väärtustele , vahele e. koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'

-- AS märksõnaga saab anda antud veerule nime
SELECT
'Peeter' AS eesnimi,
	'Paat' AS perekonnanimi,
	23 AS vanus, 
	75.45 AS kaal, 
	'Blond' AS juuksevärv

-- Tagastab praeguse kellaja ja kuupäeva mingis vaikimisi formaadis
SELECT NOW();

-- Kui tahan konkreetset osa sellest, näiteks aastat või kuud
SELECT date_part('year', NOW());

-- Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY')

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT now() + interval '1 days ago';
SELECT now() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds';

-- Tabeli loomine
CREATE TABLE student(
	id serial PRIMARY KEY, 
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- EI TOHI tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- TOHIB tühi olla
	weight numeric(5, 2) NULL,
	birthday date NULL
);
-- Tabelist kõikide ridade kõikide veergude küsimine
-- * siit tähendab, et anna kõik veerud
SELECT * FROM student;

SELECT
	first_name AS eesnimi, 
	last_name AS perekonnanimi,
	-- kuidas saaksin vanuse?
	date_part('year', now()) - date_part('year', birthday) AS vanus
FROM
	student;
	
	
--Kui tahan mingit osa ise etteantud kuupäevast
SELECT date_part 

--Kui tahan kellaajast saada näiteks minuteid
SELECT date_part 'minutes*, TIME '10:10):


	
	SELECT TIMESTAMP CONCAT('2000', '-', '01', '01')
SELECT CONCAT(first_name, '', last_name) AS täisnimi FROM student





-- Kui tahan filtreerida/otsida mingi tingimuse järgi
SELECT
*
FROM
	student
WHERE
	height = 180;
	
	
	-- Kui tahan filtreerida/otsida mingi tingimuse järgi
	-- Peeter Tamm või Mari
	SELECT 
	* 
FROM 
	student
 WHERE
 	first_name = 'Peeter' 
 	AND last_name = 'Tamm' 
	OR first_name = 'Mari'	

	
	
	
	

-- Küsi tabelist eesnimie aj perekonnanime järgi
-- mingi Peeter Tamm ja mingi Mari Maasikas
SELECT
*
FROM
	student
WHERE
	(first_name = 'Peeter' AND last_name = 'Tamm')
	OR 
	(first_name = 'Mari' AND last_name = 'Maasikas');

-- Anna mulle õpilased, kelle pikkus jääb 170-180 cm vahele
SELECT
*
FROM
	student
WHERE
	height >= 170 AND height <= 180



-- Anna mulle õpilased, kes on pikemad kui 170 cm või lühemad kui 150 cm

SELECT
*
FROM
	student
WHERE
	height >= 170 OR height <= 150;
	
-- Anna mulle õpilaste eesnimi ja pikkus, kelle sünnipäev on jaanuaris
	
	SELECT
	first_name, height
FROM
	student
WHERE
	date_part('month', birthday) = 1


-- Anna mulle õpilased, kelle middle_name on null(määramata)

SELECT 
	* 
FROM 
	student
WHERE
	middle_name IS NULL






-- Anna mulle õpilased, kelle middle_name EI OLE null
SELECT
	* 
FROM
	student
WHERE
	middle_name IS NOT NULL 


-- Anna mulle õpilased, kelle pikkus EI OLE 180 cm

SELECT
	* 
FROM
	student
WHERE
	height != 180 
	
	SELECT
	* 
FROM
	student
WHERE
-- 	height != 180;
	NOT (height = 108)
-- 	height <>180;

-- Anna mulle õpilsed, kelle pikkus on  169,145 või 180

SELECT
	* 
FROM
	student
WHERE
-- 	height = 169 OR height = 145 OR height = 180
	height IN (169,145,180)

-- Anna mulle õpilsed, kelle eesnimi on Peeter, Mari või Jüri

SELECT 
	* 
FROM 
	student
WHERE
	--first_name = 'Peeter' OR first_name = 'Mari' OR first_name = 'Kalle'
	first_name IN ('Peeter', 'Mari', 'Kalle')



NB! puudu

-- Anna mulle õpilsed, kelle eesnimi ei ole Peeter, Mari või Jüri

SELECT
	* 
FROM
	student
WHERE	
	first_name NOT IN ('Peeter','Mari','Jüri')

-- Anna mulle õpilsed, kelle sünnikuupäev on kuu esiment, neljas või seitsemes päev


SELECT
	* 
FROM
	student
WHERE
	
	date_part('day',  birthday) IN (1, 4, 7)
	


-- Kõik WHERE võrdlused jätavad välja  NULL väärtusega read
SELECT 
	* 
FROM 
	student
WHERE 
	height != 180


SELECT
	*
	FROM
	student
	WGERE
	height > 0 OR  height <=0;


-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks 
SELECT
*
FROM
	student
ORDER BY
	height 
	
	-- Kui pikkused võrdsed järjest kaalu järgi
SELECT
*
FROM
	student
ORDER BY
	height, weight 
	
	
	-- Kui tahan tagurpidises järjekorras, siis lisandub sõna DESC (Descending)
SELECT
*
FROM
	student
ORDER BY
	first_name, weight DESC, last_name 
	
	
	-- Kui tahan tagurpidises järjekorras, siis lisanudb sõna DESC (Descending)
-- Tegelikult on olemas ka ASC (Ascending), mis on vaikeväärtus
SELECT 
	* 
FROM 
	student
ORDER BY
	first_name DESC, weight ASC, last_name 

	
	

-- Anna mulle vanuse järjekorras vanemast nooremaks õpilaste pikkused,
-- mis jäävad 160 ja 170 vahele
SELECT
	height
FROM
	student
WHERE
	height >= 160 AND height <= 180
ORDER BY
	birthday 
	
	// Kui tahan otsida sõna seest mingit sõnaühendit
	SELECT
*
FROM
	student
WHERE
	first_name LIKE 'P%' -- eesnimi algab 'P'
	OR first_name Like '%ee%' -- eesnimi sisaldab ee
	OR first_name LIKE '%aan' -- eesnimi lõpeb aan


-- Tabelisse uute kirjete lisamine-- Lisati 3 rida

INSERT INTO student
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Alar', 'Allikas' ,172, 80.55, '1984-03-14', NULL), 
	('Tiiu', 'Tihane' ,171, 55.55, '1994-03-12', 'Tiia'), 
	('Mari', 'Maasikas' ,166, 56.55, '1984-05-22', 'Marleen')


-- Rea Lisamine
INSERT INTO student
	(first_name, last_name)
VALUES
	('Ivar', 'Ilus')
	
	
	--Tabelisse kirje muutmine 
	-- UPDATE lausega peab olema ETTEVAATLIK
	--ALATI PEAB KASUTAMA where- i LAUSE LÕPUS
	
--UPDATE 
--	student
--SET
--	height = 201
---- WHERE ID = 4
--WHERE
--ifrst_name = 'Kalle' AND last_name = 'Kalamees'

--Pikkuse ja kaalu muutmine, kirje muutmine kasutades WHERE lause lõpus
UPDATE 
	student
SET
	height = 199, 
	weight = 100, 
	middle_name = 'John'
	Birthday = '1988-04-05'

WHERE
	id = 4
	
	
	
	-- Muuda kõigi õpilaste pikkus ühe võrra suuremaks
	UPDATE 
	student
SET
	height = height + 1 
	
	-- Suurenda hiljem kui 1999 sündinud õpilastel sünnipäev ühe päeva võrra
	
	UPDATE 
	student
SET
	birthday = birthday + interval '1 days'
	
WHERE
	date_part('year', birthday) > 1999
	
	
	-- Kustutamisega olla ettevaatlik, ALATI KASUTA WHERE, id järgi
	
	DELETE FROM
	student
WHERE
    id = 8
	
	-- Andmette CRUD operations 
	-- Create (Insert), Read (Select), Update, Delete
	
	-- Loo uus tabel loan, millel on väljad:
	-- amount numeric(11,2), start_date, due_date, student_id, 
	

CREATE TABLE loan (
    id serial PRIMARY KEY, 
	amount numeric(11, 2) NOT NULL, 
	start_date date NOT NULL,  
	due_date date NOT NULL, 
	student_id int NOT NULL
);

-- Lisa neljale õpilasele laenud
--Kahele neist lisa veel 1 laen

INSERT INTO loan
	(amount, start_date, due_date, student_id)
VALUES
	(1000, NOW(), '2020-05-14', 6), 
	(2300, NOW(), '2021-05-14', 7), 
	(100, NOW(), '2023-05-14', 8), 
	(500, NOW(), '2024-05-14', 8), 
	(1200, NOW(), '2026-05-14', 9), 
	(1400, NOW(), '2030-05-14', 9)
	
	
	
	
	-- Anna mulle kõik õpilased koos oma laenudega
SELECT
	loan.*, student.*
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
	-- eesnimi, pereknimi, loan amount
	SELECT
	student.first_name, 
	student.last_name, 
	loan.amount
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
	

-- Anna mulle kõik õpilase koos oma laenudega
-- aga ainult sellised laenud, mis on suuremad kui 500
--järjest laenu koguse järgi suuremast väiksemaks
SELECT
	student.first_name, 
	student.last_name, 
	loan.amount
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 500
ORDER BY
	loan.amount DESC




--INNER JOIN on sellline tabelite liitmine, kus liidetakse ainult need read
--kus on võrdsed student.id = loan.student_id--ehk need read, kus tabelite vahel on seos
--ülejäänud read ignoreeritakse--
-- Anna mulle kõik õpilased koos oma laenudega

SELECT
	student.first_name,
	student.last_name,
	loan.amount
FROM
	student
INNER JOIN -- INNER JOIN on vaikimisi join, INNER Sõna võib ära jätta
	loan
	ON student.id = loan.student_id


-- Anna mulle kõik õpilased koos oma laenudega
-- aga ainult sellised laenud, mis on suuremad kui 500
--järjest laenu koguse järgi suuremast väiksemaks

SELECT
	student.first_name, 
	student.last_name, 
	loan.amount
FROM
	student
INNER JOIN -- INNER JOIN on vikimisi join, INNER Sõna võib ära jätta
	loan
	ON student.id = loan.student_id
WHERE
	loan.amount > 500
ORDER BY
	loan.amount DESC


--kogu nimekiri, kõik kel on laen või ei ole laenu INNER JOIN


-- Loo uus tabel (tabel nr3) loan_type, milles väljad name ja description

CREATE TABLE loan_type (
	id serial PRIMARY KEY, 
	name varchar(30) NOT NULL, 
	description varchar(500) NULL
);


-- UUe välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int

-- Tabeli kustutamine DROP tähendab kustutamist
DROP TABLE student;

--Lisame mõned laenu tüübid
INSERT INTO loan_type
	(name, description) 
VALUES
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')


-- SELECT tegelikult lõpus  programmi jaoks
SELECT	
	s.first_name, s.last_name, l.amount
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id

--neljas rida JOIN-ga
SELECT	
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id 
	
	-- Kolme tabeli INNER JOIN
SELECT	
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id 
	
	---INNER JOIN puhul järjekord ei ole oluline
	
SELECT	
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN
	loan AS l
	ON l.loan_type_id = lt.id
JOIN
	student AS s
	ON  s.id = l.student_id
	--- 3 variant VT Lauri 
	
	-- LEFT JOINSELECT	
	s.first_name, s.last_name, l.amount
FROM
	student  AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
	
	-- LEFT JOIN puhul võetakse joini esimesest (vasakust)tabelist kõik read
	-- ning teises (paremas) tabelis näidatakse puuduvatel kohtadel NULL
	-- LEFT JOIN puhul on järjekord väga oluline
	
	SELECT	
	s.first_name, s.last_name, l.amount
FROM
	student  AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id

	-- RIGHT, saad kasutada selle asemel LIGHT, vahetad student la loen read ära
	SELECT	
	s.first_name, s.last_name, l.amount
FROM
	student  AS s
RIGHT JOIN
	loan AS l
	ON s.id = l.student_id


	
	-- LEFT JOIN puhul on järjekord väga oluline, hakkad rohkemate väärtustega ja vähemate väärtuste pole
	
SELECT	
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student  AS s
LEFT JOIN
	loan AS l
	ON s.id = l.student_id
LEFT JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id

--Annab kõik kombinatsioonid kahe tabeli vahel
SELECT	
	s.first_name, st.first_name
FROM
	student  AS s
CROSS JOIN
	student AS st -- mitte loan AS l ?
WHERE 
	s.first_name != st.first_name


-- FULL OUTER Join on sama mis LEFT JOIN + RIGHT JOIN

SELECT	
	s.first_name, s.last_name, l.amount
FROM
	student  AS s
FULL OUTER JOIN
	loan AS l
	ON s.id = l.student_id

-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid SMS laenu
-- ja kelle laenu kogus on  üle 100 euro.
-- Tulemused järjesta  laenu võtja vanuse järgi
-- väiksemast suuremaks



SELECT	
	 s.last_name
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE 
	lt.name = 'SMS laen'
	AND l.amount > 100
ORDER BY
	s.birthday
--AGGREGATE function
--Keskmise leidmine. Jäetakse  välja read, kus height on NULL

--Aggregate functions
-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses
-- küsida mingit muud välja tabelist, sest agregaatfunktsiooni tulemus on alati 
--ainult 1 nuimber ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida



SELECT	
	 AVG(height)
FROM
	student




-- Palju on üks õpilane keskmiselt laenu võtnud
--arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (amount on NULL)
SELECT
-- 	student.first_name, 
-- 	student.last_name, 
	AVG (COALESCE (loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id


-- Keskmise leidmine, 
SELECT

	AVG (COALESCE (loan.amount, 0))
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id




SELECT

	ROUND (AVG (COALESCE (loan.amount, 0)), 0) AS "Keskmine laenusumma", 
	MIN(loan.amount) AS "Minimaalne laenusumma", 
	MAX(loan.amount) AS "Maksimaalne laenusumma"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id

______
SELECT
ROUND(AVG(COALESCE(loan.amount, 0)),0) AS "Keskmine laenusumma",
MIN(loan.amount) AS "Minimaalne laenusumma",
MAX(loan.amount) AS "Maksimaalne laenusumma",
COUNT (*) AS "Kõikide ridade arv",
COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
COUNT(student.height) AS "Mitmel õpilasel on pikkus"
from
student
LEFT JOIN
loan
ON student.id = loan.student_id

-- Kasutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on
-- GROUP BY-s ära toodud(s.first_name, s.last_namw)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees

SELECT
	s.first_name, s.last_name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
		ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name


--ERROR:  column "s.last_name" must appear in the GROUP BY clause or be used in an aggregate function
--LINE 2:  s.first_name, s.last_name, SUM(l.amount)


--Grupeerimine
SELECT
	s.first_name, 
	s.last_name,
	SUM(l.amount), 
	AVG(l.amount), 
	MIN(l.amount), 
	MAX(l.amount)
	
FROM
	student AS s
JOIN
	loan AS l
		ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name

-- Anna mulle laenude summad laenu tüüpide järgi

SELECT
	lt.name, 
	sum(l.amount)
FROM
	loan AS l
JOIN
	loan_type AS lt
		ON lt.id = l.loan_type_id
GROUP BY
	lt.name


-- Anna mulle laenude summad sünniaastate järgi
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
		ON s.id = l.student_id
GROUP BY
	date_part('year', birthday)
	
	
	-- Anna mulle laenude , mis ületavad 1000, summad sünniaastate järgi
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM
	student AS s
LEFT JOIN
	loan AS l
		ON s.id = l.student_id
GROUP BY
	date_part('year', birthday)
	--HAVING on nagu WHERE, aga peale GROUP BY kasutamist
	-- Filtreerimisel saab kasutada ainult neid välju, mis on 
	-- GROP BY`s ja agregaatfunktsioone
HAVING
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000 --- anna ainult need read, kus summa oli suurem kui 1000
	AND COUNT(l.amount) = 2  --- anna ainult need read, kus laene kokku oli 2
ORDER BY
	date_part('year', s.birthday)
	
	
	
	
	
	
	

--Tekita mingile õpilasele kaks sama tüüpi laenu
-- Anna mulle laenude summad grupeeritud õpilase ning laenu tüübi kaupa
-- mari õppelaen 2400 (1200+1200)
-- mari väikelaen 2000
-- jüri õppelaenu 20000

SELECT
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id

GROUP BY
	s.first_name, s.last_name, lt.name




--Anna mulle mitu laenu mingist tüübist on võetud ja mis on nende summad--
-- sms laenu 2 1000
--kodulaenu 1  2300
--Anna mulle mitu laenu mingist tüübist on võetud ja mis on nende summad--
-- sms laenu 2 1000
--kodulaenu 1  2300

SELECT
	 lt.name, COUNT(lt.name), SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON l.student_id = s.id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id

GROUP BY
	lt.name

-- see on variant 2
SELECT
	 lt.name, COUNT(l.amount), SUM(l.amount)
FROM
	loan_type AS lt

JOIN
	loan AS l
	ON l.loan_type_id = lt.id

GROUP BY
	lt.name
	
	
	-- lisa Lauri lahendus

--Mis aastal sündinud võtsid suurema summa rohjem laene?

-- lisa Lauri lahendus


-- Anna mulle õpilaste eesnime esitähe esinemise statistika
--ehk siis mitme õpilase eesnimi algab mingi tähega
-- m 3
-- a 2
--  l 4
-- mis sõnast, mitmendast tähest ja mitu tähte algab SUBSTRING('kala', 1, 1) 

SELECT
	SUBSTRING(s.first_name, 1, 1), COUNt(*)
FROM
	student AS s
GROUP BY
	SUBSTRING(first_name, 1, 1)
	
	-- Vt.ka Lauri lahendus
	
	----
SELECT SUBSTRING ('lauri mattus', 0, POSITION('' in 'lauri mattus')-1)


SELECT FORMAT(‘Hello %s’,’PostgreSQL’)

SELECT SUBSTRING ('lauri mattus', 0, POSITION('' in 'lauri mattus')-1)



--Subquery or Inner query or Nested query
--Anna mulle õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele
SELECT 
	first_name, last_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height))FROM student)




--Subquery or Inner query or Nested query
--Anna mulle õpilased, eesnimi on keskmise pikkusega õpilaste keskmine nimi
SELECT
	first_name, last_name
FROM
	student
WHERE 
	first_name IN

(SELECT
	middle_name
FROM
	student
WHERE
	height = (SELECT ROUND(AVG(height))FROM student))



--Lisa kaks vanimat õpilast töötajate tabelisse

INSERT INTO employee(first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student	ORDER BY birthday DESC LIMIT 2


-- 




-- Küsi kasutajalt isikukood ja valideeri kas see on õiges formaadis--Mõelge is mis piirangud isikukoodis peaks olema
-- aasta peab olema reaalne aasdta, kuu number,  kuupäeva number
--prindi välja tema sünnipäev









--See on lihtne hello world teksti päring, mis
--tagastab ühe rea 
SELECT 'Hello World'
/*
kommentaar
*/
-- Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond'
-- AS märksõnaga saab anda antud  veerule nime
SELECT
	'Peeter' AS eesnimi,
	'Paat' AS perekonnanimi, 
	23 AS vanus,
	75.45 AS kaal,
	'Blond' AS juuksevärv
	
-- Tagastab praeguse kellaaja ja kuupäeva mingis vaikimisi formaadis
	SELECT NOW();
	-- Kui tahan konkreetset os sellest, näiteks aastata või kuud
--SELECT date-part('day', )
SELECT date_part('year', time)

-- Interval laseb lisada või eemaldada mingit ajaühikut
SELECT (NOW() + interval '1 day ago');
SELECT (NOW() + interval '7 years');

SELECT (NOW() + interval '2 centuries 3 years 2 months 1 weeks 3 days 4 seconds');


CREATE TABLE student (
	id serial PRIMARY KEY,
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suuurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, --ei tohi tühi olla
	last_name varchar(64) NOT null, 
	height int NULL, -- tohib tühi olla
	weight numeric(5, 2) NULL, 
	birthday date NULL
	
) ;

-- Tabelist kõikide ridade kõikide veergude küsimine
--* siit tähendab, et anna kõik veerud
-- NB! Tärn *
SELECT * FROM student;


SELECT 
	first_name AS eesnimi,
	last_name AS perekonnanimi, 
	-- kuidas saaksin vanuse?
	date_part('year', now())- date_part('year', birthday) AS vanus
FROM
	student;


-- Kuupäeva teisendamine/ formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(), 'HH24:mm:ss DD.MM.YYYY');
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY');

SELECT 'Hello World'
--See on lihtne hello world teksti päring ,mis -- tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri)
-- selles veerus ja reas saab olema tekst


-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;
SELECT 'raud' + 'tee'; -- ei tööta PostgreSQL-s. näiteks MSSgl - s töötab
SELECT CONCAT('all','maa', 'raud', 'tee', 'jaam', '.', 2, 0, 0, 4 );
--Standard CONCAT töötab kõigis erinevates SQL serverites


CREATE TABLE student (
	id serial PRIMARY KEY,
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võrra suuurenema
	-- PRIMARY KEY (primaarvõti) tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, --ei tohi tühi olla
	last_name varchar(64) NOT null, 
	height int NULL, -- tohib tühi olla
	weight numeric(5, 2) NULL, 
	birthday date NULL
	
) ;



	
