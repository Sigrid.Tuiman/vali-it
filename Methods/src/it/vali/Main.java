//package it.vali;
//
//public class Main {
//
//    // meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust
//    // kui koodis on korduvaid koodi osasid, võiks mõelda, et et äkki peaks nende kohta tegema eraldi meetodid
//    public static void main(String[] args) {
//        printHello();
//        printHello(3);
//        printHello(2);
//        printText("Kuidas läheb?");
//        printText("Mis teed", 7);
//        printText()
//
//    }
//
//
//    // Lisame meetodi, mis prindib ekraanile Hello
//    private static void printHello(){
//        System.out.println("Hello");
//    }
//
//    // Lisame meetodi, mis prindib Hello etteantud arv kordi
//
//    static void printHello(int howManyTimes){
//        for (int i = 0; i < howManyTimes ; i++) {
//            System.out.println("Hello");
//
//        }
//
//
//    }
//
//    // Lisame meetodi, mis prindib ette antud teksti välja
//    // printText
//    static void printText(String text){
//        System.out.println(text);
//    }
//
//
//
//    // Lisame meetodi, mis prindib ette antud teksti välja ette antud arv kordi
//
//    static void printText(String text, int howManyTimes) {
//        for (int i = 0; i < howManyTimes; i++) {
//            System.out.println(text);
//        }
//
//
//    }
//    // mis prindib ette antud teksti välja ette antud arv kordi
//    // lisaks saab öelda kas tahame teha kõik tähed enne suurteks või mitte
//
//
//    static void printText(String text boolean toUpperCase) {
//        for (int i = 0; i <  ; i++) {
//            if( toUpperCase){
//                System.out.print(text.toUpperCase());
//        }
//            else {
//        System.out.print();
//    }
//
//    // Meetod OVERLOADING - mitu meetodit on sama nimega aga erinevate parameetrite kombinatsiooniga
//}


package it.vali;

public class Main {

    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud
    // kindlat funktsionaalsust.

    // Kui koodis on korduvaid koodi osasi, võiks mõelda,
    // et äkki peaks nende kohta tegema eraldi meetodi
    public static void main(String[] args) {
        printHello();
        printHello(3);
        printHello(2);
        printText("Kuidas läheb?");
        printText("Mis teed?", 7);
        printText("2", 2);
        printText(2, "2");
        printText("tere", 1, true);
        printText("tere", 1, false);
    }

    // Lisame meetodi, mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }
    }

    // Lisame meetodi, mis prindib ette antud teksti välja
    // printText
    static void printText(String text) {
        System.out.println(text);
    }

    // Lisame meetodi,
    // mis prindib ette antud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }
    }

    static void printText(int year, String text) {

        System.out.printf("%d: %s%n", year, text);
    }

    // mis prindib ette antud teksti välja ette antud arv kordi.
    // lisaks saab öelda, kas tahame teha kõik tähed enne suurteks või mitte
    static void printText(String text, int howManyTimes, boolean toUpperCase) {
        for (int i = 0; i < howManyTimes; i++) {
            if(toUpperCase) {
                System.out.println(text.toUpperCase());
            }
            else {
                System.out.println(text);
            }

        }
    }

    // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite
    //                      kombinatsiooniga
    // Meetodi ülelaadimine



}
