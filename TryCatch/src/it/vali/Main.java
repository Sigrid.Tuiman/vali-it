package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a = 0;
        try {
            String word = null;
            //tühjuse alt ei saa, annab veateate. Kui viga tekib, hüpatakse kohe lõppu catch-i
            // int b -ni ei jõuagi, tõsta int välja
            // esimene asi mis annab vea ja seejärel hüppab catsh plokki
            word.length();
            int b = 4 / a;
        }
        //Kui on mitu Catch plokki, siiis otsib esimese ploki, mis oskab
        // antud Exceptioni kinni püüda


        catch (ArithmeticException e) {
            if (e.getMessage().equals("/by zero"))
                System.out.println("Nulli ei saa jagada");

//            System.out.println(e.getMessage());
    }

    else {
            System.out.println("Esines aritmeetiline viga ");
        }


         catch (RuntimeException e)

    {
        System.out.println("Esines reaalajas esinev viga");
    }
        catch (Exception e){
                System.out.println("Juhtus mingi tundmatu viga");
    }


            // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad.
            // Mis omakorda tähendab, et püüdes kinni
            // selle üldise Exceptioni, püüame me kinni kõik Exceptionid (Exceptio e)



        catch (Exception e) {
//            e.printStackTrace();
            System.out.println("Esines viga");
        }
// Küsime kasutajalt numbri ja kui number ei ole korrektses formaadis (ei ole tegelikult number, siis ütleme mingi veateate
        Scanner scanner = new Scanner(System.in);

        boolean correctNumber;
        do {

            System.out.println("Sisesta number");
            try{
                int number  = Integer.parseInt(scanner.nextLine());
                correctNumber = true;

                catch (NumberFormatException e)
                System.out.println("Number oli vigane ");

            }
        } while (!correctNumber);

        //Loo täisarvude massiv 5 täisarvuga ning ürita sinna lisada kuues täisarv
        // Näita veateadet

        int[] numbers = new int []{1, 2, 3, 4, 5};
        try {
            numbers [5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeks, kuhu tahtsid väärtust määrata, ei eksisteeri ");

        }


    }


    }


}
