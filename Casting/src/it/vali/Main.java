package it.vali;

public class Main {

    public static void main(String[] args) {
	// Casting on teisendamine ühest arvu tüübist teise
        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse,
        // siis toimub automaatne teisendamine
        // implicit cvasting

        short b = a;

        // Kui üks numbritüüp ei pruugi mahtuda teise sisse,
        // siis peab kõigepealt ise veenduma, et see number,
        // mahub sinna teise numbritüüpi
        // ja kui mahub, siis peab ise seda teisendama (lisad(byte))
        // explicit casting

        short c = 300;
        byte d = (byte)c;

        System.out.println(d);
        // 1000 on int ja see teisendatakes juba automaatselt long-ks
        // long-st teed int-i siis veendu et long andmed ei muutu
        long e = 10000000000L;
        int f = (int) e;

        System.out.println(f);

        long g = f;
        g = c;
        g = d;

        float h = 123.23424F;
        double i = h;

        // kursor h; taha ja vasakule tekib pirn, siis rippmenüüst vali vahetus float-i

        double j = 55.111111111111;

        // antud juhul toimub ümardamine
        float k = (float)j;
        System.out.println(k);
        // kõiki reaalarve saab kirja panna E50
        double l = 12E50; // 12 * 10 ^ 50
        float m = (float)l;
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;

        System.out.println(q);
        // ei lase teha/castida järgmised 2 rida castimine toimub numbriliste tüüpide vahel
        //String r = "3";
        //int s = (int)r;

        int r = 2;
        int s = 9;
        // int / int = int
        // int / double = double
        // double / int = double
        // double + int = double
        // double / float = double

        System.out.println(r / (double) s);

        System.out.println(r / (float) s);

        float t = 12.55555F;
        double u = 23.55555;

        System.out.println(t / u);


    }
}
