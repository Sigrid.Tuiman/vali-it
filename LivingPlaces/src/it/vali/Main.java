package it.vali;

public class Main {

	public static void main(String[] args) {
		LivingPlace livingPlace = new Forest();

		Pig pig = new Pig();

		livingPlace.addAnimal(new Cat());
		livingPlace.addAnimal(new Horse());
		livingPlace.addAnimal(new Pig());
		livingPlace.addAnimal(pig);
		livingPlace.addAnimal(new Pig());
		Cow cow = new Cow();

		livingPlace.addAnimal(cow);

		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Cow");
		System.out.println();
		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.printAnimalCounts();
		livingPlace.removeAnimal("Pig");
		livingPlace.removeAnimal("Pig");

		// Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende
		// 3 meetodi sisud

	}
}














//package it.vali;
//
//public class Main {
//
//    public static void main(String[] args) {
//	  Farm farm = new Farm();
//	  Pig pig = new Pig();
//	  pig.setName("Kalle");
//
//	  farm.addAnimal(new Cat());
//	  farm.addAnimal(new Horse());
//	  farm.addAnimal(new Pig());
//	  farm.addAnimal(pig);
//	  farm.addAnimal(new Pig());
//	  Cow cow = new Cow();
//	  cow.setName("Priit");
//
//	  farm.addAnimal(cow);
//
//	//  farm.printAnimalCounts();
//		farm.removeAnimal("Cow");
//
//
//
//
//    }
//}

// Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende 3 meetodi sisud