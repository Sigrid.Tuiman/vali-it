package it.vali;

public class Main {

    public static void main(String[] args) {
	// Method Overriding ehk meetodi ülekirjutamine
        // tähendab seda, et kuskil klassis, millest antud klass pärineb,
        //päritava klassi meetodi sisu kirjutatakse pärinevas klassis üle


        Dog buldog = new Dog();
        buldog.eat();
        buldog.getAge();

        Cat siam = new Cat();
        siam.eat();
//        siam.setOwnerName("Kalle");
        siam.printInfo();

        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());


        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks 1
        // Metsloomadel printinfo võiks kirjutada Nimi: metsloomal pole nime


        WildAnimal wildAnimal = new WildAnimal();
        wildAnimal.printInfo();


        Lion lion = new Lion();
        lion.printInfo();


    }


}
