package it.vali;

public class Main {

    public static void main(String[] args) {
        //Deklareerime/defineerime tekstitüüpi (String) muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Sigrid
        String name = "Sigrid";
        String lastName = "Tuiman";

        System.out.println("Hello " + name + " " +  lastName);

    }
}
