package it.vali;

public class Cat extends Animal {

    private boolean hasFur = true;

    public void catchMouse(){
        System.out.println("Püüdsin hiire kinni");
    }

    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }


    @Override
    public void eat(){
        System.out.println("Söön hiiri");

    }
    @Override
    public void printInfo(){
        super.printInfo();
        System.out.println("Karvade olemasolu: %s%n, hasFur");
    }
}
