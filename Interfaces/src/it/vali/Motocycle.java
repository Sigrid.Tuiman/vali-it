package it.vali;

public class Motocycle extends Vehicle implements DriverInTwoWheels {
    @Override
    public void showInfo() {

    }

    @Override
    public int getMaxDistance() {
        return 0;
    }

    @Override
    public void stopDriving(int afterDistancein) {
        System.out.println("Mootorratas lõpetab sõitmise €d km pärat€n", afterDistancein);
    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal");

    }

    public String to String();

}
