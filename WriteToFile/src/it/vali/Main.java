package it.vali;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        // Try plokis otsitakse/oodatakse Exceptioneid (Erind, Erand, Viga, Erijuht)
        try {
            // FileWriter on selline klass, mis  tegeleb faili kirjutamisega
            // sellest klassist objekti loomisel antakse talle ette faili asukoha
            // faili asukohta võib olla ainult faili nimega kirjutatud: output.txt
            // sel juhul kirjutatakse faili,  mis asub samas kaustas kus meie Main.class
            // või täispika asukohaga c:\\ users\\opilane\\documents\\ output.txt
            FileWriter fileWriter = new FileWriter("C:\\Users\\opilane\\Documents\\output.txt");

            fileWriter.write("Elas metsas Mutionu");
            fileWriter.write("Elas metsas Mutionu");
            fileWriter.write("Elas metsas Mutionu\r\n");


            fileWriter.close();


            // Catch plokis püütkase kinni kindlat tüüpi exception või kõik exceptionid,
            // mis pärinevad antud exceptionist
            //
        } catch (IOException e) {
            // e.printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise
            // hierharhia/ajalugu
//            e.printStackTrace();
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");


        }

    }
}
