package it.vali;


// Interface ehk liides sunnib seda kasutavat / implementeerivat klassi omamam
// liideses kirja pandud meetodeid(sama tagastuse tüübiga ja sama parameetrite kombinatsiooniga

// Iga klass, mis interface kasutab määrab ise ära meetodi sisu
// siin sisu ei ole, sisud võivad täiesti erinevad olla
public interface Flyer {
    void fly();

}

