package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[5];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;
//        numbers[2] = 0;
//        numbers[3] = 14;
//        numbers[4] = 7;

        // eemalda siit teine number

        numbers[1] = 0;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        numbers[2] = -4;
        numbers[3] = 2;
        numbers[4] = 12;
        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);


        }

        numbers[4] = numbers[3];
        numbers[3] = 5;
        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();
        numbers[3] = numbers[4];
        numbers[4] = 0;
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();
        int x = numbers[0];
        numbers[0] = numbers[3];
        numbers[3] = x;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
// Tavalisse arraylisti võin lisada ükskõik mis tüüpi element
        // aga elemente küsides pean teadma, mis tüüpi element kus
        //täpselt asub ning pean siis selleks tüübiks ka cast-ima
        // teisendama
        List list = new ArrayList();
        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);

        int a = (int)list.get(1);
        String word = (String) list.get(0);



        int sun = 3 + (int)list.get(1);

        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));

        }
        String sentence = list.get(0) + "hommikust";




        if (String.class.isInstance(list.get(0))) {

        }
        list.add(2, "head aega");
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.34);
        //ühte listi teise listi sisse panemine

        list.addAll(3, otherList);

        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));

        }
        // contain  otsib kas on ASI LISTIS
        if(list.contains(money)){
            System.out.println("money asub listis");
        }
        // Otsib kus listis objekt asub
        System.out.printf("money asub listis", list.indexOf(money));
        // Otsib kus listis on 23
        System.out.printf("23 asub listisindeksiga %d%n", list.indexOf(23));
        // kui ei leia tagasta -1
        System.out.printf("44 asub listisindeksiga %d%n", list.indexOf(44));
        System.out.printf("44 asub listisindeksiga %d%n", list.indexOf(44));

        // Võta listist välja
        list.remove(money);
        System.out.println();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
        // prindi välja mitu elementi listis on
        System.out.println(list.size());
        list.remove(5);
        list.set(0, "tore");  // asendab
        System.out.println();
        for (int i = 0; i < list.size() ; i++) {
            System.out.println(list.get(i));

        }



    }





}