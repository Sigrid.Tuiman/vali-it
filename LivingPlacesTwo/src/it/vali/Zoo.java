package it.vali;

import java.util.Map;

public class Zoo implements LivingPlace {









    @Override
    public void addAnimal(Animal animal) {

        if(Pet.class.isInstance(animal)) {
            System.out.println("Lemmikloomi loomaaeda vastu ei võeta ");
            return;
        }


    }

    @Override
    public void printAnimalCounts() {
        for (Map.Entry<String, Integer> entry : animalCounts.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }



    }

    @Override
    public void removeAnimal(String animalType) {

        boolean animalFound = false;

        for (WildAnimal wildAnimal : animals) {
            if(WildAnimal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(wildAnimal);
                System.out.printf("Loomaaiast eemaldati loom %s%n", animalType);

                if(animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }

                animalFound = true;
                break;
            }
        }
        if(!animalFound) {
            System.out.println("Loomaaias  antud loom puudub");
        }



    }
}



//    Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende
// 3 meetodi sisud