package it.vali;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        System.out.println("Sisesta e-mail");
        Scanner scanner = new Scanner(System.in);

        String email = scanner.nextLine();
        String regex = "([a-z0-9\\._]{1,50})@[a-z0-9]{1,50}\\.([a-z]{2,10})\\.?([a-z]{2,10})?";

        if (email.matches(regex)) {
            System.out.println("e-mail on korrektne");
        } else {
            System.out.println("email ei olnud korrekne");
        }

        Matcher matcher = Pattern.compile(regex).matcher(email);
        if (matcher.matches()) {
            System.out.println("Kogu email: " + matcher.group(0));
            System.out.println("Teksti vasakulpool @ märki: " + matcher.group(1));
            System.out.println("Domeeni laiend: " + matcher.group(2));
        }

        System.out.println("Sisesta isikukood");
        scanner = new Scanner(System.in);


        String idcode = scanner.nextLine();
        regex = "^[0-9]{11}$";

        if (idcode.matches(regex)) {
            System.out.println("iskukood on korrektne");
        } else {
            System.out.println("isikukood ei ole korrekne");
        }
        matcher = Pattern.compile(regex).matcher(idcode);
        if (matcher.matches()) {
            System.out.println("isikukood: ");

        }

//        ^[1-6]([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[01])[0-9]{4}$


        // Küsi kasutajalt isikukood ja valideeri kas see on õiges formaadis
        // Mõelge ise mis piirangud isikoodis peaks olema
        // aasta peab olema reaalne aasta, kuu number, kuupäeva number
        // ja prindi välja tema süünipäev

//    lauri.mattus@gmail.com

    }
}

