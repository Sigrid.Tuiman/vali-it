package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Küsi kasutajalt 2 numbrit
        // prindi välja nende summa

        Scanner scanner = new Scanner (System.in);

        System.out.println("Sisesta esimene number");

        int a = Integer.parseInt(scanner.nextLine());

        System.out.println("Sisesta teine number");

        int b = Integer.parseInt(scanner.nextLine());

        System.out.printf("Numbrite %d ja %d summa on %d%n ", a, b, a+b);

        // Küsi nüüd 2 reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta esimene reaalarv");

        double c = Double.parseDouble(scanner.nextLine());

        System.out.println("Sisesta teine reaalarv");

        double d = Double.parseDouble(scanner.nextLine());

        System.out.printf("Numbrite %.2f ja %.2f jagatis on %.2f%n", c, d, c / d);

        // Kui ma tahan näha 0,44 asemel 0.44
        // siis ütlen eraldi ette, et kasuta USA local-i
        //System.out.println(String.format(Locale.US, format "%.2f", ...args: c/ d))
        //tere
        //Exception in thread "main" java.lang.NumberFormatException: For input string: "tere"
        //	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:68)
        //	at java.base/java.lang.Integer.parseInt(Integer.java:658)
        //	at java.base/java.lang.Integer.parseInt(Integer.java:776)
        //	at it.vali.Main.main(Main.java:15)  VT. rida, kus oon viga









    }
}
