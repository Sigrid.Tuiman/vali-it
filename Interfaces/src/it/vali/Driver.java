package it.vali;

public interface Driver {
    public void showInfo();

    int getMaxDistance();

    void stopDriving(int afterDistancein);
}
