// public - tähendab, et klass, meetod või muutuja on avalikult nähtav/ligipääsetav, väljastpoolt nähtav
// class - javas üksus, üldiselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis on ka faili nimi
// static - static meetodi ees tähendab, et seda meetodit saab välja kutsuda ilma klassist objekti loomata
// void - meetod ei tagasta midagi.
// 		meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse ja eraldatakse komaga
// String[] - tähistab stringi massiivi
// args - massiivi nimi, sisaldab käsurealt kaasa pandud parameetreid
// System.out.println - on java meetod, millega saab printida välja rida teksti
//		see, mis kirjutatakse sulgudesse (sisuliselt parameetrid), prinditakse välja ja tehakse rea vahetus
package it.vali;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");
	// write your code here
    }
}
