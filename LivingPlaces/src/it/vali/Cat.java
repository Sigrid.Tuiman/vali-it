package it.vali;

public class Cat extends Pet {
    public boolean isHasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }

    private boolean hasFur = true;

    public void catchMouse() {
        System.out.println("Püüdsin hiire kinni");
    }

    @Override
    public void eat() {
        System.out.println("Söön hiiri");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Karvade olemasolu: %s%n", hasFur);
    }
}





//package it.vali;
//
//public class Cat extends Animal {
//
//    private boolean hasFur = true;
//
//    public void catchMouse(){
//        System.out.println("Püüdsin hiire kinni");
//    }
//
//    public boolean isHasFur() {
//        return hasFur;
//    }
//
//    public void setHasFur(boolean hasFur) {
//        this.hasFur = hasFur;
//    }
//}
